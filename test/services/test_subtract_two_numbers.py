import pytest
from src.services.subtract_two_numbers import subtract_two_numbers


def test_subtract_with_zero_numbers():
  minuend, subtrahend = 0, 0
  res = subtract_two_numbers(minuend, subtrahend)
  assert res == 0


@pytest.mark.skip(reason='not implemented yet')
def test_subtract_with_two_possitive_numbers():
  minuend, subtrahend = 2, 1
  res = subtract_two_numbers(minuend, subtrahend)
  assert res == 3

