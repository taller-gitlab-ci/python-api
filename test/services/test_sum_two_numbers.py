import pytest
from src.services.sum_two_numbers import sum_two_numbers


@pytest.mark.skip(reason='avoid to check incomplete coverage')
def test_sum_with_zero_numbers():
  addend_1, addend_2 = 0, 0
  res = sum_two_numbers(addend_1, addend_2)
  assert res == 0


@pytest.mark.skip(reason='avoid to check incomplete coverage')
def test_sum_with_possitive_numbers():
  addend_1, addend_2 = 1, 2
  res = sum_two_numbers(addend_1, addend_2)
  assert res == 3


@pytest.mark.skip(reason='avoid to check incomplete coverage')
def test_sum_with_one_possitive_and_other_negative():
  addend_1, addend_2 = 1, -1
  res = sum_two_numbers(addend_1, addend_2)
  assert res == 0
