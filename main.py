from typing import Optional
from fastapi import FastAPI

from src.services.sum_two_numbers import sum_two_numbers
from src.services.subtract_two_numbers import subtract_two_numbers
from src.services.multiply_two_numbers import multiply_two_numbers
from src.services.divide_two_numbers import divide_two_numbers

app = FastAPI()


@app.get('/')
def root():
  return {'root': 'Hello'}


@app.get('/sum')
def sum_numbers(addend1: Optional[int]=0, addend2: Optional[int]=0):
  res = sum_two_numbers(addend1, addend2)
  return {'res': res}


@app.get('/subtract')
def subtract_numbers(minuend: Optional[int]=0, subtrahend: Optional[int]=0):
  res = subtract_two_numbers(minuend, subtrahend)
  return {'res': res}


@app.get('/multiply')
def multiply_numbers(factor1: Optional[int]=0, factor2: Optional[int]=0):
  res = multiply_two_numbers(factor1, factor2)
  return {'res': res}


@app.get('/divide')
def divide_numbers(factor1: Optional[int]=0, factor2: Optional[int]=0):
  res = divide_two_numbers(factor1, factor2)
  return {'res': res}
